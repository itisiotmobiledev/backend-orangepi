#!/usr/bin/python3
from time import sleep

from push_test import push_notification, data_push_notification
import threading
import paho.mqtt.client as mqtt
import wiringpi as w
import json

DEBUG = 0

HUNGRY_RATIO = 1
SLEEP_RATIO = 1
PLAY_RATIO = 1
WALK_RATIO = 1
TOILET_RATIO = 1

SPEAKER_PIN = 7
TOILET_PIN = 4

w.wiringPiSetup()

RELE = 1


# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))

    client.subscribe("team1/tamagucci")

    w.pinMode(RELE, w.OUTPUT)
    w.pinMode(TOILET_PIN, w.INPUT)


# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    print(msg.topic + " " + str(msg.payload))
    if msg.payload == b'hungry_status':
        client.publish("team1/tamagucci/hungry_status",
                       json.dumps(pet.hungry_status, indent=2))
        print(pet.hungry_status)
    elif b'sleep' in msg.payload:
        time = int(msg.payload.decode("utf-8").split()[1])
        pet.IS_SLEEPING = True
        client.publish("team1/tamagucci/check_sleep", str(pet.IS_SLEEPING))
        pet.SLEEP_TIME = time
    elif msg.payload == b'is_sleeping':
        client.publish("team1/tamagucci/check_sleep", str(pet.IS_SLEEPING))
    elif msg.payload == b'sleep_status':
        client.publish("team1/tamagucci/sleep_status",
                       json.dumps(pet.sleep_status, indent=2))
        print(pet.sleep_status)
    elif msg.payload == b'toilet_status':
        client.publish("team1/tamagucci/toilet_status",
                       json.dumps(pet.toilet_status, indent=2))
        print(pet.toilet_status)
    elif msg.payload == b'get_pet_state':
        print(pet.get_state())
        client.publish("team1/tamagucci/pet_state", pet.get_state())
    elif msg.payload == b'toilet':
        print(pet.get_state())
        play_born_melody()
        pet.toilet_status = 0
        client.publish("team1/tamagucci/toilet_status",
                       json.dumps(pet.toilet_status, indent=2))
    elif msg.payload == b'feed':
        pet.feed(10)
    else:
        print("Bad request")


client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message


def play_born_melody():
    for freq in [659, 659, 0, 659, 0, 523, 659, 0, 784, 0, 0, 0, 392, 0, 0, 0, 523, 0, 0, 392, 0, 0, 330]:
        w.softToneCreate(SPEAKER_PIN)
        w.softToneWrite(SPEAKER_PIN, freq)
        print('playing')
        w.delay(200)
        w.pinMode(SPEAKER_PIN, w.INPUT)


def generate_status_bar(title: str,
                        min_value=0,
                        current_value=0,
                        max_value=100) -> dict:
    """
    Args:
    min_value: Минимальное значение статус бара, по умполчанию = 0.
    current_value: Текущее значение статус бара, по умполчанию = 0.
    max_value: Максимальное значение статус бара, по умполчанию = 100.

    Returns:
        Возвращает словарь формата
            {'min': min_value,
            'current': current_value,
            'max': max_value}

    Raises:
        KeyError: Raises an exception.
    """
    return {'title': title,
            'min': min_value,
            'current': current_value,
            'max': max_value}


class Tamagotci:
    """ При создании класса инициализируются
        все потребности нашего будущего тамагочи.
        Будем считать что у нас создается идеальная
        ситуация, когда он сыт и
        не хочет играть/гулять/срать/спать.
    """

    def __init__(self, name: str):
        self.__name = name
        self.IS_SLEEPING = False
        self.SLEEP_TIME = 0
        self.__hungry_status = generate_status_bar('hungry_status')
        self.__sleep_status = generate_status_bar('sleep_status')
        self.__toilet_status = generate_status_bar('toilet_status')
        if not DEBUG:
            play_born_melody()

    def __str__(self):
        return self.__name

    def __del__(self):
        print(f'{self.__str__()} DIED!!!')
        data_push_notification(title="IM DEAD!",
                               body="☠☠☠☠☠",
                               type=f'{self.__str__()} DIED')

    # Блок атрибутов (сеттеры и геттеры)
    @property
    def hungry_status(self) -> dict:
        return self.__hungry_status

    @hungry_status.setter
    def hungry_status(self, value: int):
        if value > 100:
            self.__hungry_status['current'] = 100
        elif value < 0:
            self.__hungry_status['current'] = 0
        else:
            self.__hungry_status['current'] = value

    @property
    def sleep_status(self) -> dict:
        return self.__sleep_status

    @sleep_status.setter
    def sleep_status(self, value: int):
        if value > 100:
            self.__sleep_status['current'] = 100
        elif value < 0:
            self.__sleep_status['current'] = 0
        else:
            self.__sleep_status['current'] = value

    @property
    def toilet_status(self) -> dict:
        return self.__toilet_status

    @toilet_status.setter
    def toilet_status(self, value: int):
        if value > 100:
            self.__toilet_status['current'] = 100
        elif value < 0:
            self.__toilet_status['current'] = 0
        else:
            self.__toilet_status['current'] = value

    def get_state(self) -> json:
        return json.dumps([self.hungry_status,
                           self.sleep_status,
                           self.toilet_status], indent=2)

    # Блок функций удовлетворения потребностей
    # Исполняются и изменяют текущие статусы потребностей
    def feed(self, food: int):
        # TODO: звуковые эффекты при приёме пищи
        w.digitalWrite(RELE, w.HIGH)
        sleep(1)
        w.digitalWrite(RELE, w.LOW)
        print("feeding")
        self.hungry_status = self.hungry_status['current'] - food
        client.publish("team1/tamagucci/hungry_status",
                       json.dumps(self.hungry_status, indent=2))

    def toilet(self):
        """ По умолчанию геркон даёт 1 на вход.
            Если замыкаем, то 0.
        """
        while 1:
            print("WAITING FOR THE TOILET")
            while w.digitalRead(TOILET_PIN) != 0:
                i = 0
            print("OKAY, IM DONE")
            play_born_melody()
            self.toilet_status = 0
            sleep(5)


def life_cycle(tamagotci, mqtt_client):
    """
    Механизм жизненного цикла.
    В реальном времени изменяет потребности тамагочи
    """
    while True:
        # Если не спит
        try:
            if not tamagotci.IS_SLEEPING:
                # Отсылает в соответствующий канал состояния
                # TODO: Реализация прогулки и игры
                # TODO: Слишком сильно срёт пушами
                if tamagotci.hungry_status['current'] >= 80:
                    data_push_notification(body="Your tamagotchi wants eat!",
                                           title="IM HUNGRY!",
                                           type="hungry")
                if tamagotci.toilet_status['current'] >= 80:
                    data_push_notification(body="Your tamagotchi wants to defecate!",
                                           title="PLEASE!",
                                           type="toilet")
                if tamagotci.sleep_status['current'] >= 80:
                    data_push_notification(body="Your tamagotchi wants to sleep!",
                                           title="IM TIRED!",
                                           type="sleep")
                print(tamagotci.get_state())
                tamagotci.hungry_status = tamagotci.hungry_status['current'] + (1 * HUNGRY_RATIO)
                mqtt_client.publish("team1/tamagucci/hungry_status",
                                    json.dumps(tamagotci.hungry_status, indent=2))
                sleep(5)
                tamagotci.toilet_status = tamagotci.toilet_status['current'] + (1 * TOILET_RATIO)
                mqtt_client.publish("team1/tamagucci/toilet_status",
                                    json.dumps(tamagotci.toilet_status, indent=2))
                sleep(5)
                tamagotci.sleep_status = tamagotci.sleep_status['current'] + (1 * SLEEP_RATIO)
                mqtt_client.publish("team1/tamagucci/sleep_status",
                                    json.dumps(tamagotci.sleep_status, indent=2))
                sleep(5)
                print(tamagotci.get_state())
                if 1 < (tamagotci.hungry_status['current'] // 100 +
                        tamagotci.toilet_status['current'] // 100 +
                        tamagotci.sleep_status['current'] // 100):
                    tamagotci.__del__()  # DIE MF DIE
                    del tamagotci
            else:
                print("Sleepin_life_cycle")
                # print(tamagotci.get_state())
                # TODO: звуковые эффекты при сне
                # TODO: Ограничить увеличение голода и туалета если всё еще не проснулся,
                #  а статус скоро достигнет 100%
                # TODO: Пофиксить паблиши о статусе.
                #  Сейчас отправляются только при окончании сна.
                print("SLEEPING")
                for second in range(tamagotci.SLEEP_TIME, 0, -1):
                    print("SENDING NEW SLEEP STATUS")
                    tamagotci.sleep_status = tamagotci.sleep_status['current'] - (1 * SLEEP_RATIO)
                    client.publish("team1/tamagucci/sleep_status",
                                   json.dumps(tamagotci.sleep_status, indent=2))
                    sleep(1)
                    print("SENDING NEW HUNGRY STATUS")
                    tamagotci.hungry_status = tamagotci.hungry_status['current'] + (1 * HUNGRY_RATIO)
                    client.publish("team1/tamagucci/hungry_status",
                                   json.dumps(tamagotci.hungry_status, indent=2))
                    sleep(1)
                    tamagotci.toilet_status = tamagotci.toilet_status['current'] + (1 * TOILET_RATIO)
                    print("SENDING NEW TOILET STATUS")
                    client.publish("team1/tamagucci/toilet_status",
                                   json.dumps(tamagotci.toilet_status, indent=2))
                    sleep(1)
                    print(f'SLEEPING {second} second...')
                print("WAKING UP")
                tamagotci.IS_SLEEPING = False
                client.publish("team1/tamagucci/check_sleep", tamagotci.IS_SLEEPING)
                data_push_notification(body="SLEEP_STATUS",
                                       title="IM WAKING UP",
                                       type="waking_up")
        except UnboundLocalError:
            main()


def main():
    if DEBUG:
        print("SPAWN NEW TAMAGOTCI")
        global pet
        pet = Tamagotci(name="TOM")

        client.connect("192.168.14.254", 1883, 60)

        pet.sleep_status = 80
        pet.toilet_status = 80
        pet.hungry_status = 80

        pet_life_cycle = threading.Thread(target=life_cycle, args=(pet, client))
        pet_life_cycle.daemon = True
        pet_life_cycle.start()

        pet_toilet_cycle = threading.Thread(target=pet.toilet)
        pet_toilet_cycle.daemon = True
        pet_toilet_cycle.start()

        client.loop_start()
        pet_toilet_cycle.join()
        pet_life_cycle.join()

    else:
        pet = Tamagotci(name="TOM")
        client.connect("192.168.14.254", 1883, 60)

        pet_life_cycle = threading.Thread(target=life_cycle, args=(pet, client))
        pet_life_cycle.daemon = True
        pet_life_cycle.start()

        mqtt_loop = threading.Thread(target=client.loop_forever)
        mqtt_loop.daemon = True
        mqtt_loop.start()

        pet_toilet_cycle = threading.Thread(target=pet.toilet)
        pet_toilet_cycle.daemon = True
        pet_toilet_cycle.start()

        mqtt_loop.join()
        pet_toilet_cycle.join()
        pet_life_cycle.join()


if __name__ == '__main__':
    main()
