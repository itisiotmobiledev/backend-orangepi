FROM debian:latest
MAINTAINER SRETeams
RUN apt-get update
RUN apt-get -y install git-all build-essential dh-make dpkg debconf debhelper fakeroot
RUN apt-get -y upgrade
RUN mkdir /root/git
WORKDIR /root/git
RUN git clone https://gitlab.com/ialx84/wiringop.git /root/git/wiringop
WORKDIR /root/git/wiringop
RUN dpkg-buildpackage -us -uc
WORKDIR /root/git
RUN dpkg -i *.deb
#RUN gpio readall
RUN apt-get install -y python3-pip python3-dev python3-setuptools swig
WORKDIR /root/git
RUN git clone https://gitlab.com/ialx84/wiringop-python.git
RUN cp -R wiringop/* wiringop-python/WiringPi/
WORKDIR /root/git/wiringop-python
RUN git clone https://keep_on_rail@bitbucket.org/itisiotmobiledev/backend-orangepi.git tamagucci
RUN ls -la
WORKDIR /root/git/wiringop-python/tamagucci
RUN ls -la
EXPOSE 1883
RUN pip3 install -r requirements.txt
CMD ["python3", "tamagotci.py"]