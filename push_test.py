import json

import requests

from keys import server_key, push_token

host = 'https://fcm.googleapis.com/fcm/send'


def push_notification(msg_title: str,
                      msg_body: str,
                      **kwargs):
    headers = {'Authorization': f'key={server_key}',
               'Content-Type': 'application/json'}

    payload = {
        "to": push_token,
        "collapse_key": "type_a",
        "notification": {
            "body": msg_body,
            "title": msg_title
        },
        "data": kwargs
    }

    requests.post(host, json.dumps(payload), headers=headers)
    print(json.dumps(payload))


def data_push_notification(**kwargs):
    headers = {'Authorization': f'key={server_key}',
               'Content-Type': 'application/json'}

    payload = {
        "to": push_token,
        "collapse_key": "type_a",
        "data": kwargs
    }

    requests.post(host, json.dumps(payload), headers=headers)
    print(json.dumps(payload))
