import paho.mqtt.client as mqtt
import wiringpi as w
from time import sleep

LED = 7
RELE = 1


# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe("team1/tamagucci")

    w.wiringPiSetup()
    w.pinMode(LED, w.OUTPUT)
    w.pinMode(RELE, w.OUTPUT)


# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    w.digitalWrite(LED, w.HIGH)
    w.digitalWrite(RELE, w.HIGH)
    print(msg.topic + " " + str(msg.payload))
    sleep(1)
    w.digitalWrite(LED, w.LOW)
    w.digitalWrite(RELE, w.LOW)


client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

client.connect("192.168.14.254", 1883, 60)

# Blocking call that processes network traffic, dispatches callbacks and
# handles reconnecting.
# Other loop*() functions are available that give a threaded interface and a
# manual interface.
client.loop_forever()
